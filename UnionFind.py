#Union find class (not needed in the second solution which works best)
class UF:
	def __init__(self, n):
		self.n = n
		self.p = [-1 for i in range(n)]
	
	def P(self, i):
		while self.p[i] >= 0:
			if self.p[self.p[i]] >= 0:
				self.p[i] = self.p[self.p[i]]
			i = self.p[i]
		return i

	def U(self, a, b):
		pa = self.P(a)
		pb = self.P(b)
		if pa == pb:
			return
		if(self.p[pa] > self.p[pb]):
			self.p[pb] += self.p[pa]
			self.p[pa] = pb
		else:
			self.p[pa] += self.p[pb]
			self.p[pb] = pa
	
	def S(self, i):
		return -self.p[self.P(i)]
