#Problem description can be changed to solve other problems
#CAUTION in some places the number of pieces and the size of end cube are hard coded 

allPieceBlocks = [
	#corner
	[
		[0, 0, 0],
		[1, 0, 0],
		[0, 1, 0],
		[0, 0, 1]
	],
	#weird t
	[
		[0, 0, 0],
		[1, 0, 0],
		[2, 0, 0],
		[1, 1, 0]
	],
	#long l
	[
		[0, 0, 0],
		[1, 0, 0],
		[2, 0, 0],
		[2, 1, 0]
	],
	#zig zag
	[
		[0, 0, 0],
		[1, 0, 0],
		[1, 1, 0],
		[2, 1, 0]
	],
	#weird
	[
		[0, 0, 0],
		[1, 0, 0],
		[1, 1, 1],
		[1, 1, 0]
	],
	#weird
	[
		[0, 0, 0],
		[1, 0, 0],
		[1, 1, 1],
		[1, 1, 0]
	],
	# small l 
	[
		[0, 0, 0],
		[1, 0, 0],
		[1, 1, 0]
	]
	]
