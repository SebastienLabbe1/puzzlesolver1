#!/usr/bin/env python3
import visualize
import numpy
from UnionFind import UF
from pieces import allPieceBlocks
from itertools import product

#Tool to solve a puzzle where given some building blocks one has to create a solid cube with no holes
# to understand better run code and see soluiton each color represents a piece in the solution

class Block:
	def __init__(self, p):
		self.p = p

	def getPos(self, origin, orientation):
		return origin + numpy.array([self.p[orientation.a[i]] * orientation.o[i] for i in range(3)])

class Orientation:
	def __init__(self, right, forward):
		self.a = [abs(right) - 1, abs(forward) - 1, 5 - abs(right) - abs(forward)]
		self.o = [1 if right > 0 else -1,\
			1 if forward > 0 else -1,\
			1 if (right * forward > 0) == (abs(right) % 3 + 1 == abs(forward)) else -1]

	def all():
		return [Orientation(k * i, j * ((i%3)+1)) for i in range(1,4) for k in [-1, 1] for j in [-1,1]] \
		+ [Orientation(k * ((i%3)+1), j * i) for i in range(1,4) for k in [-1, 1] for j in [-1,1]]

class Piece:
	def __init__(self, blocks, origin = [0, 0, 0], orientation = Orientation(1, 2)):
		self.blocks = blocks.copy()
		self.origin = numpy.array(origin)
		self.orientation = orientation

	def setPos(self, origin, orientation):
		self.origin = numpy.array(origin)
		self.orientation = orientation
	
	def inPos(self, origin, orientation):
		return Piece(self.blocks, origin, orientation)
	
	def getBlockPositions(self):
		return [b.getPos(self.origin, self.orientation) for b in self.blocks]

	def __hash__(self):
		return hash(tuple(tuple(b.p) for b in self.blocks))

	def intersect(self, piece):
		pos1 = self.getBlockPositions()
		pos2 = piece.getBlockPositions()
		for p1 in pos1:
			for p2 in pos2:
				if (p1 == p2).all():
					return True
		return False
	
	def fromPieceBlocks(allPieceBlocks):
		allPieces = []
		for i,pieceBlocks in enumerate(allPieceBlocks):
			origin = [3*i, 0, 0]
			allPieces.append(Piece([Block(b) for b in pieceBlocks], origin))
		return allPieces
	
	def allOrientations(self):
		allOr = Orientation.all()
		allPieces = []
		for i in range(len(allOr)):
			origin = [3*i, 0, 0]
			allPieces.append(self.inPos(origin,allOr[i]))
		return allPieces
	
	def inBounds(self, a = 0, b = 3):
		for pos in self.getBlockPositions():
			if (pos < a).any() or (pos >= b).any():
				return False
		return True

	def allPosSpaces(self, spaces, turn = True):
		poses = []
		allOri = Orientation.all()
		if not turn:
			allOri = [Orientation(1, 2)]
		for origin in spaces:
			for orientation in allOri:
				poses.append(self.inPos(origin, orientation))
		return poses

	def allPosCube(self, cube):
		poses = []
		allOri = Orientation.all()
		a = 0
		b = len(cube)
		for i in range(a, b):
			for j in range(a, b):
				for k in range(a, b):
					if cube[i][j][k]:
						continue
					origin = [i, j, k]
					for orientation in allOri:
						p = self.inPos(origin, orientation)
						if p.inBounds(a, b):
							poses.append(p)
		return poses

	def allPosInBounds(self, a = 0, b = 3):
		poses = []
		allOri = Orientation.all()
		for i in range(a, b):
			for j in range(a, b):
				for k in range(a, b):
					origin = [i, j, k]
					for orientation in allOri:
						p = self.inPos(origin, orientation)
						if p.inBounds(a, b):
							poses.append(p)
		return poses
	
	def allUniqueInBounds(self, turn, a = 0, b = 3):
		poses = []
		allOri = Orientation.all()
		if not turn:
			allOri = [Orientation(1, 2)]
		for origin in product(range(a, b), range(a, b), range(a, b)):
			for orientation in allOri:
				p = self.inPos(origin, orientation)

				#check Bounds
				BP = p.getBlockPositions()
				good = False
				for pos in BP:
					if (pos < a).any() or (pos >= b).any():
						break
				else:
					good = True
				if not good:
					continue

				BPS = {ijk_to_p(x) for x in BP}
				good = False
				for _,_,prevBPS in poses:
					if len(BPS.difference(prevBPS)) == 0:
						break
				else:
					good = True
				if not good:
					continue
						
				poses.append([p, BP, BPS])
		return poses


def ijk_to_p(ijk):
	return ijk[0] + ijk[1] * 3 + ijk[2] * 9

def p_to_ijk(p):
	return (p % 3, (p // 3) % 3, p // 9)
	
def filledSpaces(ps):
	cube = [[[0] * 3 for i in range(3)] for j in range(3)]
	for p in ps:
		for pos in p.getBlockPositions():
			if (pos < 0).any() or (pos > 2).any():
				return None
			cube[pos[0]][pos[1]][pos[2]] += 1
	return cube


def emptySpaces(ps, a = 0, b = 3):
	cube = [[[0] * b for i in range(b)] for j in range(b)]
	for p in ps:
		for pos in p.getBlockPositions():
			if (pos < a).any() or (pos >= b).any():
				return []
			if cube[pos[0]][pos[1]][pos[2]]:
				return []
			cube[pos[0]][pos[1]][pos[2]] = 1
	spaces = []
	for i in range(a, b):
		for j in range(a, b):
			for k in range(a, b):
				if not cube[i][j][k]:
					spaces.append((i, j, k))
	return spaces

def emptySpaces2(cube):
	spaces = []
	for p in range(27):
		if not cube[p]:
			i, j, k = p_to_ijk(p)
			spaces.append((i, j, k))
	return spaces
	

def canCube(ps):
	cube = [[[0] * 3 for i in range(3)] for j in range(3)]
	for p in ps:
		for pos in p.getBlockPositions():
			if (pos < 0).any() or (pos > 2).any():
				return False
			if cube[pos[0]][pos[1]][pos[2]]:
				return False
			cube[pos[0]][pos[1]][pos[2]] = 1
	return True

def manDist(x, y):
	return sum(abs(x[i] - y[i]) for i in range(len(x)))

def goodHoles(spaces, minSize = 3):
	n = len(spaces)
	spacesS = {spaces[i]:i for i in range(n)}
	uf = UF(n)
	for i in range(n):
		for j in range(i):
			if manDist(spaces[i], spaces[j]) == 1:
				uf.U(i, j)
	for i in range(n):
		if uf.S(i) < minSize:
			return False
	return True
	
def isCube(ps):
	cube = [[[0] * 3 for i in range(3)] for j in range(3)]
	for p in ps:
		for pos in p.getBlockPositions():
			if (pos < 0).any() or (pos > 2).any():
				return False
			if cube[pos[0]][pos[1]][pos[2]]:
				return False
			cube[pos[0]][pos[1]][pos[2]] = True 
	if any([any([any(x) for x in xx]) for xx in cube]):
		return False
	return True

#attempt 1

def visPieces(ps):
	vis = visualize.FasterVisualizer()
	diag = numpy.array([0.5, 0.5, 0.5])
	for i,p in enumerate(ps):
		ic = i % len(visualize.tupcolors)
		color = (visualize.tupcolors[ic][0], visualize.tupcolors[ic][1], visualize.tupcolors[ic][2], 0.5)
		for pos in p.getBlockPositions():
			vs, fs = visualize.gen_mesh_cube(pos * 1.5 + diag, diag)
			vis.add_mesh(vs, fs, color)
	vis.show_plot()


def getCube(curPiecesPrev, index, allPieces, cubePrev):
	#Counting
	global counter, sols
	counter += 1

	#Local copy
	curPieces = curPiecesPrev.copy()

	if not counter % 100:
		print("get Cube calls : ", counter, "current pieces : ", len(curPieces), "total solutions : ", len(sols))

	#If nothing else to add
	if index == len(allPieces):
		sols.append(curPieces.copy())
		return True

	#Free spaces and some checks
	spaces = emptySpaces2(cubePrev)

	if len(spaces) == 0 or not goodHoles(spaces):
		return False

	curPieces.append(Piece([]))
	
	#Add next piece
	cube = cubePrev.copy()
	allPoses = allPieces[index].allPosSpaces(spaces, index > 0)
	allBP = [p.getBlockPositions() for p in allPoses]
	allBPS = [{ijk_to_p(x) for x in bp} for bp in allBP]
	if index < 2: 
		print(" " * (index * 10), "len is ", len(allPoses))
	for i,nextPiece in enumerate(allPoses):
		curBPS = allBPS[i]

		#check for redundancy
		good = True
		for j in range(i):
			prevBPS = allBPS[j]
			if len(curBPS.difference(prevBPS)) == 0:
				good = False
				break
		if not good:
			continue
		if index < 2: 
			print(" " * (index * 10), i)

		#copy of cube
		for k in range(len(cube)):
			cube[k] = cubePrev[k]

		#check if piece collides
		good = True
		for pos in allBP[i]:
			if (pos < 0).any() or (pos > 2).any():
				good = False
				break
			p = ijk_to_p(pos)
			if cube[p]:
				good = False
				break
			cube[p] = True
		if not good:
			continue

		#set next piece and recursive call
		curPieces[-1] = nextPiece
		getCube(curPieces, index + 1, allPieces, cube)
	return False

def fullCube(allPieces):
	return getCube([], 0, allPieces, [False] * 27)

#attempt 2
def getCube2(allPoses, index, ichosen, taken):
	global counter, sols, solsf
	counter += 1

	if index == len(allPoses):
		sol = [allPoses[i][j][0] for i,j in enumerate(ichosen)]
		sols.append(sol)
		solf = [allPoses[i][j] for i,j in enumerate(ichosen)]
		solsf.append(solf)
		return True

	if not counter % 10000:
		print("get Cube calls : ", counter, "current pieces : ", index, "total solutions : ", len(sols))
	
	localTaken = taken.copy()
	if index < 1: 
		print(" " * (index * 10), "len is ", len(allPoses[index]))
	for i, p in enumerate(allPoses[index]):
		if len(p[2].intersection(taken)) > 0:
			continue
		if index < 1: 
			print(" " * (index * 10), i)
		ichosen[index] = i
		getCube2(allPoses, index + 1, ichosen, taken.union(p[2]))
	return False



def fullCube2(allPoses):
	return getCube2(allPoses, 0, [0] * len(allPoses), set())


def cubesEqual(cube1, cube2):
	for i, j, k in product(range(3), range(3), range(3)):
		if cube1[i][j][k] != cube2[i][j][k]:
			return False
	return True

def rotate(p, ori):
	return [p[ori.a[0]] if ori.o[0] > 0 else 2 - p[ori.a[0]],
	p[ori.a[1]] if ori.o[1] > 0 else 2 - p[ori.a[1]],
	p[ori.a[2]] if ori.o[2] > 0 else 2 - p[ori.a[2]]]
	

def rotateCube(cube, orientation):
	newcube = [[[0] * 3 for i in range(3)] for j in range(3)]
	for i, j, k in product(range(3), range(3), range(3)):
		ri, rj, rk = rotate([i, j, k], orientation) 
		newcube[i][j][k] = cube[ri][rj][rk]
	return newcube

	

def compareCubes(cube1, cube2):
	if cubesEqual(cube1, cube2):
		return True
	for i, j in [(2, 3), (3, 1)]:
		ori = Orientation(i, j)
		if cubesEqual(cube1, rotateCube(cube2, ori)):
			return True
	return False

def solToHashCube(sol):
	cube = [[[0] * 3 for i in range(3)] for j in range(3)]
	for p in sol:
		phash = hash(p[0])
		for pos in p[1]:
			cube[pos[0]][pos[1]][pos[2]] = phash
	return cube

def removeSame(solsf, sols):
	cubes = [solToHashCube(sol) for sol in solsf]
	ccubes = []
	uSols = []
	for i,cube in enumerate(cubes):
		for ccube in ccubes:
			if compareCubes(cube, ccube):
				break
		else:
			uSols.append(sols[i])
			ccubes.append(cubes[i])
	return uSols

counter = 0
sols = []
solsf = []

allOrientations = Orientation.all()


allPieces = Piece.fromPieceBlocks(allPieceBlocks)

allPoses = []

for i, p in enumerate(allPieces):
	UIB = p.allUniqueInBounds(i > 0)
	allPoses.append(UIB)

fullCube2(allPoses)

#fullCube(allPieces)

print(len(sols))
usols = removeSame(solsf[:], sols)
print(len(usols))

cube = solToHashCube(solsf[0])

compareCubes(cube,cube)

for sol in usols:
	visPieces(sol)
